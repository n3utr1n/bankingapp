import Dao.AccountDao;
import Dao.UserDao;
import Dao.UserDao;
import Model.Account;
import Model.User;
import Service.IOService;


import java.util.List;
import java.util.Random;

public class BankingApp {

    private IOService ioService;
    private UserDao userDao;
    private AccountDao accountDao;

    public BankingApp() {
        ioService = new IOService();
        userDao = new UserDao();
        accountDao = new AccountDao();
    }

    public void start() {
        while (true) {
            ioService.displayUnauthenticatedUserMenu();
            String userInput = ioService.getUserInput();

            processUnloggedUser(userInput);
        }
    }

    private void processUnloggedUser(String userInput) {
        if (userInput.equalsIgnoreCase("1")) {
            register();
        } else if (userInput.equalsIgnoreCase("2")) {
            User loggedInUser = login();
            if (loggedInUser != null) {
                loggedInUser.setLoggedIn(true);
                while (loggedInUser.isLoggedIn()) {
                    ioService.displayAuthenticatedUserMenu();
                    userInput = ioService.getUserInput();

                    processLoggedInUser(userInput, loggedInUser);
                }
            }
        }
    }


    private void processLoggedInUser(String userInput, User user) {
        System.out.println("======================================================================");
        System.out.println("Processing logged-in user input! ");
        switch (userInput) {
            case "1": {
                viewPortfolio(user);
                break;
            }
            case "2": {
                transferMoneyFrom(user);
                break;
            }
            case "3": {
                depositCash(user);
                break;
            }
            case "4": {
                createDebitAccount(user);
                break;
            }
            case "5": {
                createCreditAccount(user);
                break;
            }
            case "6": {
                transferMoneyWithinAccountsOf(user);
                break;
            }
            case "0": {
                logOut(user);
                break;
            }
        }

    }

    private void transferMoneyWithinAccountsOf(User user) {
        ioService.displayAccounts(user.getAccountList());
        String sourceAccountIndex = ioService.getUserInput(); //sau getField("account");

        String amountToTransfer = ioService.getField("amount to transfer");

        ioService.displayAccounts(user.getAccountList());
        String destinationAccountIndex = ioService.getUserInput();

        if (!validateSameUserTransfer(sourceAccountIndex, amountToTransfer, destinationAccountIndex, user.getAccountList())) {
            ioService.displayValidationFieldError();
            return;
        }
        performTransfer(user, amountToTransfer, sourceAccountIndex, destinationAccountIndex);

    }

    private void performTransfer(User user, String amountToTransfer, String sourceAccountIndex, String destinationAccountIndex) {
        int amount = Integer.parseInt(amountToTransfer);
        Account source = user.getAccountByUserIndex(sourceAccountIndex);
        Account destination = user.getAccountByUserIndex(destinationAccountIndex);
        source.decreaseBy(amount);
        destination.increaseBy(amount);
        userDao.updateEntity(user);

    }

    private boolean validateSameUserTransfer(String sourceAccountIndex, String amountToTransfer, String destinationAccountIndex, List<Account> accountList) {

        try {
            int sourceIndex = Integer.parseInt(sourceAccountIndex);
            int destinationIndex = Integer.parseInt(destinationAccountIndex);

            int amount = Integer.parseInt(amountToTransfer);
            if (sourceIndex <= 0 || sourceIndex > accountList.size() ||
                    destinationIndex <= 0 || destinationIndex > accountList.size() ||
                    sourceIndex == destinationIndex) {
                ioService.errorMessageField("account index");
                return false;
            }
            Account sourceAccount = accountList.get(sourceIndex - 1);

            if (sourceAccount.getAmount() < amount || amount <= 0) {
                ioService.errorMessageField("account amount");
                return false;
            }

        } catch (Exception exception) {
            ioService.errorMessageField("numeric");
            return false;
        }
        return true;
    }


    private void transferMoneyFrom(User user) {
        //identify the source account
        ioService.displayAccounts(user.getAccountList());
        String userAccountIndex = ioService.getUserInput(); //sau getField("account");

        //set transfer ammount
        String amountToTransfer = ioService.getField("amount to transfer");

        //set trasfer IBAN
        String destinataryIban = ioService.getField("Destinatary IBAN");

        //validate Iban, trasfered amount (+ currency)
        if (!validate(userAccountIndex, amountToTransfer, destinataryIban, user.getAccountList())) {
            ioService.displayValidationFieldError();
            return;
        }
        //perform transfer
        performTransfer(user.getAccountByUserIndex(userAccountIndex), amountToTransfer, destinataryIban);

    }

    private void performTransfer(Account sourceAccount, String amountToTransfer, String destinataryIban) {
        Account destonationAccoint = accountDao.findByIban(destinataryIban);
        int amount = Integer.parseInt(amountToTransfer);
        destonationAccoint.increaseBy(amount);
        sourceAccount.decreaseBy(amount);
        accountDao.updateEntity(sourceAccount, destonationAccoint);
        ioService.displayConfirmationMessage();

    }

    private boolean validate(String userAccountIndex, String amountToTransfer, String destinataryIban, List<Account> accountList) {

        try {
            int accountIndex = Integer.parseInt(userAccountIndex);
            int amount = Integer.parseInt(amountToTransfer);
            if (accountIndex <= 0 || accountIndex > accountList.size()) {
                ioService.errorMessageField("account index");
                return false;
            }
            Account account = accountList.get(accountIndex - 1);
            System.out.println("sursa" + account);
            if (account.getAmount() < amount || amount <= 0) {
                ioService.errorMessageField("account amount");
                return false;
            }
            Account accountByIban = accountDao.findByIban(destinataryIban);
            if (accountByIban == null) {
                ioService.errorMessageField("account IBAN");
                return false;
            }

        } catch (Exception exception) {
            ioService.errorMessageField("numeric");
            return false;
        }
        return true;
    }

    private void viewPortfolio(User user) {
        ioService.displayAccountsInformation(user);

    }

    private void logOut(User user) {

        ioService.displayLogOut();
        String userInput = ioService.getUserInput();
        if (userInput.equalsIgnoreCase("1")) {
            user.setLoggedIn(false);
        } else if (userInput.equalsIgnoreCase("2")) {
            System.out.println("======================================================================");
            System.out.println("NOT LOGGED OUT !");
        }
    }

    private void depositCash(User user) {
        ioService.displayAccounts(user.getAccountList());
        String bankAccountIndex = ioService.getUserInput();
        String amount = ioService.getField("amount");
        System.out.println("The user wants to add " + amount + " in bank account: " + bankAccountIndex + "-1");
        addAmount(amount, user.getAccountByUserIndex(bankAccountIndex));
    }

    private void addAmount(String amount, Account account) {
        long currentAmount = account.getAmount();
        long newAmount = currentAmount + Integer.parseInt(amount);
        account.setAmount(newAmount);
        accountDao.updateEntity(account);
        ioService.displayConfirmationMessage();
    }


    private void createDebitAccount(User user) {
        String accountName = ioService.getField("account name");
        String currency = ioService.getField("currency");

        Account account = new Account();
        account.setAccountName(accountName);
        account.setCurrency(currency);
        account.setIban(generatedIban());
        account.setUser(user);
        user.addAccount(account);
        accountDao.saveEntity(account);

        System.out.println("======================================================================");
        System.out.println("Your Debit account - " + accountName + " - has been created!");
        System.out.println("The IBAN of your account is: " + generatedIban());

    }

    private void createCreditAccount(User user) {
        String accountName = ioService.getField("account name");
        String currency = ioService.getField("currency");
        String creditLimit = ioService.getField("credit limit");
        long creditLimitAsLong = Long.parseLong(creditLimit);

        Account account = new Account();
        account.setAccountName(accountName);
        account.setCurrency(currency);
        account.setAmount(creditLimitAsLong);
        account.setIban(generatedIban());
        account.setUser(user);
        account.setCredit(true);
        account.setCreditLimitAmount(creditLimitAsLong);
        user.addAccount(account);
        accountDao.saveEntity(account);

        System.out.println("======================================================================");
        System.out.println("Your Debit account - " + accountName + " - has been created!");
        System.out.println("The IBAN of your account is: " + generatedIban());

    }

    private String generatedIban() {
        String iban = "RO12INGB";
        Random random = new Random();
        for (int index = 0; index < 16; index++) {
            iban = iban + random.nextInt(10);
        }
        return iban;
    }


    private User login() {
        String username = ioService.getField("username");
        String password = ioService.getField("password");
        System.out.println("======================================================================");
        System.out.println("Trying to log in with: " + username + " & " + password);

        User user = userDao.findByUsername(username);
        if (user == null) {
            System.out.println("======================================================================");
            System.out.println("Username does not exist! ");

            return null;
        }


        System.out.println(user + " attending log-in ");
        System.out.println("======================================================================");
        if (user.getPassword().equals(password)) {
            System.out.println("You are now loged-in! ");

            return user;
        } else {
            System.out.println("======================================================================");
            System.out.println("The Username and password are invalid, please try again!");

            return null;

        }
    }

    private void register() {
        String firstName = ioService.getField("first name");
        String lastName = ioService.getField("last name");
        String cnp = ioService.getField("cnp");
        String email = ioService.getField("email");
        String username = ioService.getField("username");
        String password = ioService.getField("password");

        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setCNP(cnp);
        user.setEmail(email);
        user.setUsername(username);
        user.setPassword(password);

        System.out.println("======================================================================");
        System.out.println("You have successfully been registered! Please log-in!");

        System.out.println(user);
        userDao.saveEntity(user);

    }
}
