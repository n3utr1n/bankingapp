package Service;
import Model.Account;
import Model.User;

import java.util.List;
import java.util.Scanner;

public class IOService {

    private Scanner scanner = new Scanner(System.in);

    public void displayUnauthenticatedUserMenu(){
        System.out.println("======================================================================");
        System.out.println("Hello, please type one of the following options: ");
        System.out.println("1 - Register ");
        System.out.println("2 - Login ");
        System.out.println("======================================================================");
        System.out.println("Your answer: ");

    }

    public String getUserInput(){
        return scanner.nextLine();
    }

    public Integer getUserValue(){
        return scanner.nextInt();
    }

    public String getField(String field) {
        System.out.println("======================================================================");
        System.out.print("Please insert "+ field + ": ");
        return scanner.nextLine();
    }

    public void displayAuthenticatedUserMenu() {
        System.out.println("======================================================================");
        System.out.println("Please select one of the following options: ");

        System.out.println("1 - View portfolio and balance ");
        System.out.println("2 - Transfer money ");
        System.out.println("3 - Deposit cash at ATM ");
        System.out.println("4 - Create debit account ");
        System.out.println("5 - Create credit account ");
        System.out.println("6 - Transfer money within your accounts ");
        System.out.println("0 - Log Out ");
        System.out.println("======================================================================");
        System.out.println("Your answer: ");
    }

    public void displayAccounts(List<Account> accountList) {
        System.out.println("======================================================================");
        System.out.println("Please select one of the following accounts: ");
        for(int index = 0; index < accountList.size(); index ++){
            System.out.println((index+1) + " - "+ accountList.get(index).getAccountName());
        }
        System.out.println("======================================================================");
        System.out.println("Your answer is: ");
    }

    public void displayLogOut(){
        System.out.println("======================================================================");
        System.out.println("Please confirm you want to log out! ");
        System.out.println("1 - YES");
        System.out.println("2 - NO");
        System.out.println("======================================================================");
        System.out.println("Your answer: ");
    }

    public void displayConfirmationMessage(){
        System.out.println("======================================================================");
        System.out.println("Your action has successfully been completed !");
    }

    public void displayAccountsInformation(User user) {
        System.out.println("======================================================================");
        System.out.println("Account information for " + user.getFullName());
        System.out.println("Account name \t\t\t Amount \t\t\t IBAN \t\t\t\t Type");
        for(Account account : user.getAccountList()){
            String accountType = account.isCredit()?"credit":"debit";
            System.out.println(account.getAccountName() + "\t\t\t" + account.getAmount()
                    + account.getCurrency() + "\t\t\t" + account.getIban()+"\t" +accountType);
        }
        displayConfirmationMessage();
    }

    public void displayValidationFieldError() {
        System.out.println("======================================================================");
        System.out.println("One of the fields contains invalid data! Please thy again!");
    }

    public void errorMessageField(String fieldName) {
        System.out.println("The " + fieldName +" field is not valid! ");
    }
}
